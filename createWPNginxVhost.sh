#!/bin/bash

read -p "Enter the existing username to which you want to add a virtual host: " username
egrep "^$username:" /etc/passwd >/dev/null
        if [ $? -eq 0 ]; then
        echo "awesome the $username exists!"

	read -p "Enter domain to be added without www : " vhostName

echo "
#
# vhost-$username.conf
#
server {
    listen       80;
    server_name $vhostName;
    access_log /home/$username/logs/access.log;
    error_log /home/$username/logs/error.log;
    root   /home/$username/public_html;
    index index.php  index.html index.htm;

location ~ /wp-content/cache/minify.*\.js\$ {
    types {}
    default_type application/x-javascript;
}
location ~ /wp-content/cache/minify.*\.css\$ {
    types {}
    default_type text/css;
}

rewrite ^/wp-content/cache/minify.*/w3tc_rewrite_test\$ /wp-content/plugins/w3-total-cache/pub/minify.php?w3tc_rewrite_test=1 last;

set \$w3tc_enc \"\";

if (-f \$request_filename\$w3tc_enc) {
    rewrite (.*) \$1\$w3tc_enc break;
}

rewrite ^/wp-content/cache/minify/(.+/[X]+\.css)\$ /wp-content/plugins/w3-total-cache/pub/minify.php?test_file=\$1 last;
rewrite ^/wp-content/cache/minify/(.+\.(css|js))\$ /wp-content/plugins/w3-total-cache/pub/minify.php?file=\$1 last;

set \$cache_uri \$request_uri;

if (\$request_method = POST) {
        set \$cache_uri 'no cache';
    }
    if (\$query_string != \"\") {
        set \$cache_uri 'no cache';
    }

if (\$request_uri ~* \"(\/wp-admin\/|\/xmlrpc.php|\/wp-(app|cron|login|register|mail)\.php|wp-.*\.php|index\.php|wp\-comments\-popup\.php|wp\-links\-opml\.php|wp\-locations\.php)\") {
        set \$cache_uri \"no cache\";
    }

if (\$http_cookie ~* \"comment_author|wordpress_[a-f0-9]+|wp\-postpass|wordpress_logged_in\") {
        set \$cache_uri 'no cache';
    }


location / {
            try_files /wp-content/w3tc/pgcache/\$cache_uri/_index.html \$uri \$uri/ /index.php?q=\$uri&\$args;
        }
        
        location ~* \.(xml|ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|css|rss|atom|js|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)\$ {
                try_files       \$uri =404;
                expires         max;
                access_log      off;
        }
        

     error_page  404              /404.html;
    location = /404.html {
        root   /home/$username/public_html;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /home/$username/public_html;
    }

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    location ~ \.php\$ {
        root           /home/$username/public_html;
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME   \$document_root\$fastcgi_script_name;
        include        fastcgi_params;
    }
    
    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    location ~ /\.ht {
        deny  all;
    	access_log      off;
        log_not_found   off;

	}
}
" > /etc/nginx/conf.d/vhost-$username.conf

echo 'created virtual host'

    service nginx reload
    echo "nginx reloaded"

echo 'Website config created. Live long and prosper'
else
		echo "please add user first"
		exit 1
	fi
